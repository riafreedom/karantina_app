package karantina_app.transportation_message;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private static final String SMS_NUMBER = "13033";
    private String message;
    private String language;
    private MenuItem menuDetails;
    private MenuItem menuAbout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String savedName  = getSharedPreferences("my_cache", 0).getString("name", "");
        final String savedAddress  = getSharedPreferences("my_cache", 0).getString("address", "");
        language  = getSharedPreferences("my_cache", 0).getString("language", "el_GR");

        final EditText nameEditText = (EditText) findViewById(R.id.editText10);
        nameEditText.setText(savedName);

        nameEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SharedPreferences.Editor editor = getSharedPreferences("my_cache", 0).edit();
                editor.putString("name", s.toString());
                editor.commit();
            }
        });

        final EditText addressEditText = (EditText) findViewById(R.id.editText9);
        addressEditText.setText(savedAddress);


        addressEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SharedPreferences.Editor editor = getSharedPreferences("my_cache", 0).edit();
                editor.putString("address", s.toString());
                editor.commit();
            }
        });

        final Button button1 = findViewById(R.id.button2);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendMessage(1, nameEditText.getText().toString(), addressEditText.getText().toString());
            }
        });

        final Button button2 = findViewById(R.id.button3);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendMessage(2, nameEditText.getText().toString(), addressEditText.getText().toString());
            }
        });

        final Button button3 = findViewById(R.id.button4);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendMessage(3, nameEditText.getText().toString(), addressEditText.getText().toString());
            }
        });

        final Button button4 = findViewById(R.id.button5);
        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendMessage(4, nameEditText.getText().toString(), addressEditText.getText().toString());
            }
        });

        final Button button5 = findViewById(R.id.button6);
        button5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendMessage(5, nameEditText.getText().toString(), addressEditText.getText().toString());
            }
        });

        final Button button6 = findViewById(R.id.button7);
        button6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendMessage(6, nameEditText.getText().toString(), addressEditText.getText().toString());
            }
        });


        // Use the cashed language if no language selected
        setLanguage();

        final ImageButton imageButtonGR = (ImageButton) findViewById(R.id.imageButton);

        imageButtonGR.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                language = "el_GR";
                SharedPreferences.Editor editor = getSharedPreferences("my_cache", 0).edit();
                editor.putString("language", language);
                editor.commit();
                setLanguage();
            }
        });

        final ImageButton imageButtonUS = (ImageButton) findViewById(R.id.imageButton2);

        imageButtonUS.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                language = "en_US";
                SharedPreferences.Editor editor = getSharedPreferences("my_cache", 0).edit();
                editor.putString("language", language);
                editor.commit();
                setLanguage();
            }
        });

    }

    private void showDetails() {

        String title = "";
        String message = "";

        if(language.equals("el_GR")) {
            title = "Πληροφορίες για τις μετακινήσεις";
            message = "<html><b><u>Μεμονωμένες μετακινήσεις (Τύπου Β)</u></b><br><br>" +
                    "<b>1.</b> Μετάβαση σε φαρμακείο ή επίσκεψη στον γιατρό, εφόσον αυτό συνιστάται μετά από σχετική επικοινωνία.<br><br>" +
                    "<b>2.</b> Μετάβαση σε εν λειτουργία κατάστημα προμηθειών αγαθών πρώτης ανάγκης (σούπερ μάρκετ, μίνι μάρκετ), όπου δεν είναι δυνατή η αποστολή τους.<br><br>" +
                    "<b>3.</b> Μετάβαση στην τράπεζα, στο μέτρο που δεν είναι δυνατή η ηλεκτρονική συναλλαγή.<br><br>" +
                    "<b>4.</b> Κίνηση για παροχή βοήθειας σε ανθρώπους που βρίσκονται σε ανάγκη ή συνοδεία ανηλίκων μαθητών από/προς το σχολείο.<br><br>" +
                    "<b>5.</b> Μετάβαση σε τελετή κηδείας υπό τους όρους που προβλέπει ο νόμος ή μετάβαση διαζευγμένων γονέων ή γονέων που τελούν σε διάσταση που είναι αναγκαία για τη διασφάλιση της επικοινωνίας γονέων και τέκνων, σύμφωνα με τις κείμενες διατάξεις.<br><br>" +
                    "<b>6.</b> Σωματική άσκηση σε εξωτερικό χώρο ή κίνηση με κατοικίδιο ζώο, ατομικά ή ανά δύο άτομα, τηρώντας στην τελευταία αυτή περίπτωση την αναγκαία απόσταση 1,5 μέτρου.</html>";
        }
        else if (language.equals("en_US")) {
            title = "Information about transportation";
            message = "<html><b><u>Extraordinary Movement Permit (Type B)</u></b><br><br>" +
                    "<b>1.</b> Going to the pharmacy or visiting a Medical Doctor, in the case that this is recommended after consultation.<br><br>" +
                    "<b>2.</b> Going to a supply store in operation (super markets, mini markets etc), which can not ship or deliver its goods.<br><br>" +
                    "<b>3.</b> Going to the bank, when electronic transactions are not possible.<br><br>" +
                    "<b>4.</b> Going to help people in need or escorting underage students to/from school.<br><br>" +
                    "<b>5.</b> Going to a funeral ceremony under the conditions provided by law; or movement of divorced or legally separated parents in the context of parental responsibility, custody, or visiting rights in accordance with the applicable provisions.<br><br>" +
                    "<b>6.</b> Physical exercise in an open space or for a pet’s needs; individually or up to 2 people. In the latter case, a minimum distance of 1.5 meters must be kept at all times.</html>";
        }

        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(Html.fromHtml(message));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void showAbout() {

        String title = "";
        String message = "";

        if(language.equals("el_GR")) {
            title = "Σχετικά με την εφαρμογή";
            message = "<html>Αυτή η εφαρμογή δημιουργήθηκε για να κάνει τη ζωή των ανθρώπων ευκολότερη, σε αυτές τις δύσκολες στιγμές.<br><br>" +
                    "Αποσκοπεί στη γρήγορη συμπλήρωση του μηνύματος μετακίνησης, για τη μετέπειτα αποστολή του από εσάς στο <b>13033</b>.<br><br>" +
                    "Κανένα προσωπικό στοιχείο δε συλλέγεται.<br><br>" +
                    "Να είστε ασφαλείς!</html>";
        }
        else if (language.equals("en_US")) {
            title = "About";
            message = "<html>This application was created in order to make people's life easier, during these troubled times.<br><br>" +
                    "It aims at the quick generation of the transportation SMS, in order to be sent to <b>13033</b> by you.<br><br>" +
                    "No personal data are collected.<br><br>" +
                    "Stay safe!</html>";
        }

        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(Html.fromHtml(message));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void forwardToSMSClient() {

        try {
            Uri sms_uri = Uri.parse("smsto:" + SMS_NUMBER);
            Intent sms_intent = new Intent(Intent.ACTION_SENDTO, sms_uri);
            sms_intent.putExtra("sms_body", message);

            if (sms_intent.resolveActivity(getPackageManager()) != null) {
                startActivity(sms_intent);
            }
            else {
                if (language.equals("el_GR")) {
                    Toast.makeText(MainActivity.this, "Αποτυχία αποστολής. H προεπιλεγμένη εφαρμογή αποστολής SMS δεν βρέθηκε.", Toast.LENGTH_SHORT).show();
                } else if (language.equals("en_US")) {
                    Toast.makeText(MainActivity.this, "Failed to send. The default application for sending SMS was not found.", Toast.LENGTH_SHORT).show();
                }
            }
        }
        catch (Exception e){
            if (language.equals("el_GR")) {
                Toast.makeText(MainActivity.this, "Αποτυχία αποστολής. " + e.getMessage(), Toast.LENGTH_SHORT).show();
            } else if (language.equals("en_US")) {
                Toast.makeText(MainActivity.this, "Failed to send." + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void sendMessage(int transportation, String name, String address) {

        if (name.isEmpty() || address.isEmpty()) {
            if (language.equals("el_GR")) {
                Toast.makeText(getApplicationContext(), "Το ονοματεπώνυμο και η διεύθυνση είναι υποχρεωτικά πεδία.", Toast.LENGTH_LONG).show();
            } else if (language.equals("en_US")) {
                Toast.makeText(getApplicationContext(), "FullName and address are required fields.", Toast.LENGTH_LONG).show();
            }
            return;
        }

        message = "" + transportation + " " + name + " " + address;

        forwardToSMSClient();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.extra, menu);
        menuDetails = menu.findItem(R.id.details);
        menuAbout = menu.findItem(R.id.about);

        if(language.equals("el_GR")) {
            menuDetails.setTitle("Πληροφορίες για τις μετακινήσεις");
            menuAbout.setTitle("Σχετικά με την εφαρμογή");
        }
        else if (language.equals("en_US")) {
            menuDetails.setTitle("Information about transportation");
            menuAbout.setTitle("About");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.details:
                showDetails();
                return true;
            case R.id.about:
                showAbout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setLanguage() {

        final Button button1 = findViewById(R.id.button2);
        final Button button2 = findViewById(R.id.button3);
        final Button button3 = findViewById(R.id.button4);
        final Button button4 = findViewById(R.id.button5);
        final Button button5 = findViewById(R.id.button6);
        final Button button6 = findViewById(R.id.button7);
        final EditText nameEditText = (EditText) findViewById(R.id.editText10);
        final EditText addressEditText = (EditText) findViewById(R.id.editText9);
        final TextView textView5 = (TextView) findViewById(R.id.textView5);
        final TextView textView8 = (TextView) findViewById(R.id.textView8);

        if (language.equals("el_GR")) {
            button1.setText("Φαρμακείο/Γιατρός");
            button2.setText("Κατάστημα προμηθειών");
            button3.setText("Τράπεζα");
            button4.setText("Παροχή βοήθειας");
            button5.setText("Τελετή/Διαζευγμένοι γονείς");
            button6.setText("Άθληση/Κατοικίδιο");
            textView8.setText("Στείλε μήνυμα για μετακίνηση:");
            textView5.setText("Βεβαιωθείτε ότι λάβατε την απάντηση, μετά την αποστολή του μηνύματος στο 13033, προτού πραγματοποιήσετε τη μετακίνησή σας.");
            nameEditText.setHint("Ονοματεπώνυμο");
            addressEditText.setHint("Διεύθυνση");

            if(menuDetails != null) {
                menuDetails.setTitle("Πληροφορίες για τις μετακινήσεις");
            }

            if(menuAbout != null) {
                menuAbout.setTitle("Σχετικά με την εφαρμογή");
            }

        } else if (language.equals("en_US")) {
            button1.setText("Pharmacy/Doctor");
            button2.setText("Supply store");
            button3.setText("Bank");
            button4.setText("Provide assistance");
            button5.setText("Ceremony/Divorced parents");
            button6.setText("Exercise/Pet");
            textView8.setText("Send message for transportation:");
            textView5.setText("Make sure you received the reply, after sending the message to 13033, before proceeding with your transportation.");
            nameEditText.setHint("FullName");
            addressEditText.setHint("Address");

            if(menuDetails != null) {
                menuDetails.setTitle("Information about transportation");
            }

            if(menuAbout != null) {
                menuAbout.setTitle("About");
            }
        }
    }

}
